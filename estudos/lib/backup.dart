import 'package:flutter/material.dart';

class Home extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,//alinhamento dos widget pela colunas
        //crossAxisAlignment: CrossAxisAlignment.stretch,//oculpa a largura total dos widgets
        mainAxisAlignment: MainAxisAlignment.end,//alinhamento do componente em relação aos outros
        children: <Widget>[
          FlutterLogo(size: 100.0, colors: Colors.red,),//adicona o icom do flutter
          FlutterLogo(size: 100.0, colors: Colors.amber,),
          FlutterLogo(size: 100.0, colors: Colors.green,),
          FlutterLogo(size: 100.0),
        ],
      ),
      
    );
  }

}
