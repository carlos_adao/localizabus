import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:estudos/increment/increment-controller.dart';
import 'package:flutter/material.dart';
import 'package:estudos/SegundaTela/segundaTela.dart';


class IncrementWidget extends StatefulWidget {
  @override
  _IncrementWidgetState createState() => _IncrementWidgetState();
}

class _IncrementWidgetState extends State<IncrementWidget> {
  @override
  Widget build(BuildContext context) {
    final IncrementController bloc =
        BlocProvider.of<IncrementController>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Bloc"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.navigate_next),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=>SegundaTelaWidget()));
              }),
        ],
      ),
      body: Center(
          child: StreamBuilder(
        stream: bloc.outCount,
        builder: (BuildContext context, AsyncSnapshot snap) {
          return Text("Tocou no botão add ${snap.data} vezes");
        },
      )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: bloc.increment,
      ),
    );
  }
}
