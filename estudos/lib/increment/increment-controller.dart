import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';

class IncrementController implements BlocBase{


  //fluxo da variavel count
  var _countController = BehaviorSubject<int>(seedValue: 0);


  //get usado para retorna a variavel count
  Stream<int> get outCount => _countController.stream;

  //método usado para expor o input do controller
  Sink<int> get inCount => _countController.sink;

  void increment(){
    inCount.add(_countController.value+1);
  }

  @override
  void dispose() {
    _countController.close();
  }

}