import 'dart:html';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:estudos/increment/increment-controller.dart';
import 'package:flutter/material.dart';

class TerceiraTelaWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final IncrementController bloc =
        BlocProvider.of<IncrementController>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Segunga Tela'),
      ),
      body: StreamBuilder(
          stream: bloc.outCount,
          builder: (BuildContext context, AsyncSnapshot snap) {
            return Center(
              child: RaisedButton(
                child: Text("valor do contador: ${snapshot.data}"),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TerceiraTelaWidget()));
                },
              ),
            );
          }),
    );
  }
}
