import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'global.dart';

class Usuario {
  int id;
  String nome, profissao, status;

  Usuario(
      {this.id,
      this.nome,
      this.profissao,
      this.status}); //Metodo statico para decode do json

  //class para decodificar json
  factory Usuario.fromJson(Map<String, dynamic> json) {
    return Usuario(
        id: json["id"],
        nome: json["nome"],
        profissao: json["profissao"],
        status: json["status"]);
  }

  //método de conexão com a api
  Future<List<Usuario>> fetchUsuarios(http.Client client) async {
    String url = "endereco api";
    final response = await client.get(url);
    if (response.statusCode == 200) {
      Map<String, dynamic> map = json.decode(response.body);
      if (map["result"] == "ok") {
        final usurio = map['data'].cast<Map<String, dynamic>>();
        final listOfUsuarios = await usurio.map<Usuario>((json) {
          return Usuario.fromJson(json);
        }).toList();
        return listOfUsuarios;
      } else {
        return [];
      }
    } else {
      throw Exception("Falha ao carregar lista");
    }
  }
}
