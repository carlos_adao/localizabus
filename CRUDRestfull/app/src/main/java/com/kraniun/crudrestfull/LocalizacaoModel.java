package com.kraniun.crudrestfull;

import android.util.Log;

import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by estagio-nit on 03/01/18.
 */

public class LocalizacaoModel {

    private String BASE_URL = "http://13.58.21.219/localizacaoapi/localizacao/";
    private RestTemplate restTemplate = new RestTemplate();


    //usado no metodo get
    public List<Localizacao> findAll(){
        try{
            return restTemplate.exchange(
                    BASE_URL,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<Localizacao>>() {
                    }
            ).getBody();

        }catch (Exception e){
            return null;
        }
    }

    //usado no metodo get
    public List<Localizacao> localizacaoFind(int id){
        try{
            return restTemplate.exchange(
                    BASE_URL + "find/"+id,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<Localizacao>>() {
                    }
            ).getBody();

        }catch (Exception e){
            return null;
        }
    }

    /*usado no metodo post*/
    public boolean create(Localizacao localizacao){


        try{
            Map<String, String> values =  new HashMap<>();
            values.put("cod", String.valueOf(localizacao.getCod()));
            values.put("att", localizacao.getAtt());
            values.put("lat", String.valueOf(localizacao.getLat()));
            values.put("log", String.valueOf(localizacao.getLog()));
            values.put("provider", localizacao.getProvider());

              String json =  "{\"Localizacao\":[{\n" +
                " \"cod\": \""+localizacao.getCod()+"\",\n" +
                " \"att\": \""+localizacao.getAtt()+"\",\n" +
                " \"lat\": \""+localizacao.getLat()+"\",\n" +
                " \"log\": \""+localizacao.getLog()+"\",\n" +
                " \"provider\": \""+localizacao.getProvider()+"\"\n" +
                " }]}";

            Log.i("test", json);
            JSONObject jsonObject = new JSONObject(values);

            HttpHeaders headers = new  HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(json, headers);

            Log.i("Chamando", String.valueOf(restTemplate.postForEntity(BASE_URL, entity, null)));


            return true;
        }catch (Exception e){

            return false;
        }
    }


}
