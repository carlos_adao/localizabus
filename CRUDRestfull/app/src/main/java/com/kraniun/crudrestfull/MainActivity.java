package com.kraniun.crudrestfull;

import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button btn_envia;
    private Button btn_delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            List<Localizacao> listaLocalizacao = new HttpRequestFindAll().execute().get();


        } catch (Exception e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
        btn_envia = (Button)findViewById(R.id.btn_envia);
        btn_envia.setOnClickListener(btn_enviaListener);

        btn_delete = (Button)findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(btn_enviaListener);

    }

    public void insert() {
        Localizacao l = new Localizacao();
        l.setCod(8);
        l.setAtt("08/01/2017 20:50");
        l.setLat(14.786880);
        l.setLog(12.989980);
        l.setProvider("test2");

        try {
            boolean result = new HttpRequestAdd().execute(l).get();
        } catch (Exception e) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
    }


    /*classe que execulta a thread em background para o metodo get*/
    private class HttpRequestFindAll extends AsyncTask<Void, Void, List<Localizacao>> {


        @Override
        protected List<Localizacao> doInBackground(Void... voids) {

            LocalizacaoModel localizacaoModel = new LocalizacaoModel();
            return localizacaoModel.findAll();
        }

        @Override
        protected void onPostExecute(List<Localizacao> localizacaoList) {
            super.onPostExecute(localizacaoList);
        }
    }


    /*class executada em background para o metodo post*/
    private class HttpRequestAdd extends AsyncTask<Localizacao, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Localizacao... localizacaos) {
            LocalizacaoModel localizacaoModel = new LocalizacaoModel();

            return localizacaoModel.create(localizacaos[0]);
        }
    }

    /*class executada em background para o metodo post*/
    private class HttpRequestDelete extends AsyncTask<Localizacao, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Localizacao... localizacaos) {
            LocalizacaoModel localizacaoModel = new LocalizacaoModel();

            return localizacaoModel.create(localizacaos[0]);
        }
    }





    /*Implementação do click do botão*/
    private View.OnClickListener btn_enviaListener = new View.OnClickListener() {
        public void onClick(View v) {

            Log.i("Envia","testndo");
            insert();
            switch (v.getId()) {
                case R.id.btn_envia:

                    insert();
                    break;

                case R.id.btn_delete:


                    break;


            }
        }
    };


}
