package com.kraniun.crudrestfull.Tests;


import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Observable;
import java.util.Observer;

public class RestServiceClient extends Observable {

    private String URL = "http://192.168.100.2:8080";

    public void callService(Observer backCommunicator){

        addObserver(backCommunicator);

        AsyncTask<String, Void, String> restCallTask =
                new AsyncTask<String, Void, String>() {

                    @Override
                    protected String doInBackground(String... paramss) {

                        RestTemplate restTemplate = new RestTemplate();

                        HttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
                        HttpMessageConverter stringHttpMessageConverternew = new StringHttpMessageConverter();

                        restTemplate.getMessageConverters().add(formHttpMessageConverter);
                        restTemplate.getMessageConverters().add(stringHttpMessageConverternew);

                        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
                        map.add("first_name", "Stancho");
                        map.add("last_name", "Stanchev");

                        return
                                restTemplate.postForObject(paramss[0],map, String.class);

                    }

                    @Override
                    protected void onPostExecute(String result) {

                        Log.i("onPostExecute:...", result);
                        RestServiceClient.this.setChanged();
                        RestServiceClient.this.notifyObservers(result);
                        RestServiceClient.this.deleteObservers();
                    }
                };

        restCallTask.execute(URL);
    }

}