package com.kraniun.crudrestfull;

import java.io.Serializable;

/**
 * Created by estagio-nit on 03/01/18.
 */

public class Localizacao implements Serializable {
    int cod;
    String att;
    double lat;
    double log;
    String provider;

    public Localizacao() {

    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getAtt() {
        return att;
    }

    public void setAtt(String att) {
        this.att = att;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLog() {
        return log;
    }

    public void setLog(double log) {
        this.log = log;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
