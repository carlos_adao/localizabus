package com.kraniun.localizabus.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kraniun.localizabus.Auxiliares.PermissionUtils;
import com.kraniun.localizabus.Localizacao.Localizacao;
import com.kraniun.localizabus.Localizacao.Service.LocalizacaoControle;
import com.kraniun.localizabus.R;


import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private static final String TAG = "Map Activity";
    protected GoogleMap map;
    private SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    MarkerOptions markerOptions;
    boolean primeira_vez = true;
    LocalizacaoControle lc;
    int cont = 11;
    private boolean insert = true;
    private int cod; //variavel para guardar o código da viagem
    private List<Localizacao> localizacaolist;
    private int posList = 0;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        cod = Integer.parseInt(getIntent().getStringExtra("cod"));
        getLocation(cod);//chama o controller para pegar as localizações no servidor
        requesteGoogleClient();

        /*Criando um marcador para o mapas
        markerOptions = new MarkerOptions();


        setContentView(R.layout.activity_main);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        requesteGoogleClient();*/
    }

    /*Método usado para obter as localizações de acordo com o código da linha*/
    private void getLocation(int cod) {
        lc = new LocalizacaoControle();
        /*Verifica se alguma localização foi retornada do servidor*/
        if (lc.getLocalizacaoId(cod) != null) {
            Log.i("MSG: ", "O servidor está retornando uma localização Não Nula");
            insert = false;
        } else {
            Log.i("MSG: ", "O servidor está retornando uma localização Nula");
        }

        localizacaolist = lc.getLocalizacao();

    }

    //Método usado para configurar o Google cliente para abertura do mapa
    private void requesteGoogleClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        Log.d(TAG, "onMapReady: " + map);
        this.map = map;

        // Configura o tipo do mapa
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Conecta no Google Play Services
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        // Para o GPS
        stopLocationUpdates();
        // Desconecta
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {

        Log.d(TAG, "Conectado no Google Play Services!");
        // Inicia o GPS
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        toast("Conexão interrompida.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        toast("Erro ao conectar: " + connectionResult);
    }

    // Atualiza a coordenada do mapa
    private void setMapLocation() {

        Localizacao l = localizacaolist.get(posList);
        posList++;
        if (map != null && l != null) {
            LatLng latLng = new LatLng(l.getLat(), l.getLog());

            //focaliza no mapa quando é a primeira vez da utilização
            if (primeira_vez) {
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 17);
                map.animateCamera(update);
                primeira_vez = false;

            }

            TextView text = (TextView) findViewById(R.id.text);
            String s = String.format("Última atualização %s", DateFormat.getTimeInstance().format(new Date()));
            s += String.format("\nLat/Lnt %f/%f, provider: %s", l.getLat(), l.getLog(), l.getProvider());
            text.setText(s);

            String dt = DateFormat.getTimeInstance().format(new Date());
            Log.i("Insert", String.valueOf(insert));


            Localizacao loc = new Localizacao();
            loc.setCod(cont);
            loc.setAtt(dt);
            loc.setLat(l.getLat());
            loc.setLog(l.getLog());
            loc.setProvider(l.getProvider());

            //if (insert) {
            //lc.insert(loc, getApplicationContext());
            insert = false;
            //} else {
            // lc.upgrade(loc, getApplicationContext());
            // }
            cont++;


            // Setting position for the marker
            markerOptions.position(latLng).title("Terminal Urbano").snippet("Via Cidade Nova");


            // Setting custom icon for the marker
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_bus));


            // Adding the marker to the map
            map.clear();
            map.addMarker(markerOptions);


            // Desenha uma bolinha vermelha
            /*CircleOptions circle = new CircleOptions().center(latLng);
            circle.fillColor(Color.RED);
            circle.radius(25); // Em metros
            map.clear();
            map.addCircle(circle);*/
        }
    }

    private void toast(String s) {
        Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT).show();
    }


    /*Método usado para iniciar a leitura do gps*/
    protected void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates()");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1 * 100);
        mLocationRequest.setFastestInterval(1 * 100);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        chama();
    }

    protected void stopLocationUpdates() {
        Log.d(TAG, "stopLocationUpdates()");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged(): " + location);
        // Nova localizaçao: atualiza a interface
        //setMapLocation(location);
        //setMapLocation();
    }

    public void chama() {
        Thread x = new Thread() {
            public void run() {
                while (true) {
                    setMapLocation();
                    try {
                        // do something here
                        //display3.setText("System On");

                        Log.d(TAG, "local Thread sleeping");
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "local Thread error", e);
                    }

                }
            }
        };

    }
}
