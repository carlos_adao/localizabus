package com.kraniun.localizabus.Localizacao;

import android.util.Log;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

public class LocalizacaoModel {

    private static final String TAG = "Localizacao Model";
    private RestTemplate restTemplate = new RestTemplate();

    public LocalizacaoModel() { }

    public int enviaLocalizacaoInWS(Localizacao loc) {
        String url = "http://sistema.octopuscode.com.br/loc/insert";
        String requestJson = loc.getJsonInsertLocalizacao();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);

        try {
            int result = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    entity,
                    new ParameterizedTypeReference<Integer>() {
                    }
            ).getBody();
            return result;

        } catch (Exception e) {
            Log.i("Erro", String.valueOf(e));
            return 0;
        }
    }

    //Método usado para receber uma localização do servidor
    public Localizacao recebeLocalizacaoInWS(String linha) {
        String url = "http://sistema.octopuscode.com.br/loc/retorna";
        String requestJson = "{\"linha\": \""+linha+"\" "+"}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        try {
            Localizacao result = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    entity,
                    new ParameterizedTypeReference<Localizacao>() {
                    }
            ).getBody();
            Log.i("MSG", result.toString());
            return result;
        } catch (Exception e) {
            Log.i("Erro", String.valueOf(e));
            return null;
        }

    }

    //Método para receber todas as localizações de um determinado dia do servidor
    public ArrayList<Localizacao> recebeAllLocalizacaoInWS(String linha) {
        String url = "http://sistema.octopuscode.com.br/loc/locDate";
        String requestJson = "{\"linha\": \""+linha+"\" "+"}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        try {
            ArrayList<Localizacao> result = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    entity,
                    new ParameterizedTypeReference<ArrayList<Localizacao>>() {
                    }
            ).getBody();
            return result;
        } catch (Exception e) {
            Log.i("Erro", String.valueOf(e));
            return null;
        }

    }

}
