package com.kraniun.localizabus.Coneccao;

import android.util.Log;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by estagio-nit on 21/09/17.
 */

public class ConexaoHttp {

    public static String URL;//localização da url com os daddos
    static java.net.URL url;

    public static String getURL() {
        return URL;
    }

    public static void setURL(String URL) {
        ConexaoHttp.URL = URL;

    }

    /*retorna uma lista de dados no formato json*/
    public static JSONObject carregarArquivoJson(int inst) {
        HttpURLConnection conexao = null;
        try {

            switch (inst) {
                case 1:

                    /*Estabelece a conexão com settigs*/
                    conexao = connectarSettings(URL);

                    break;

                case 2:
                    /*Estabelece conexão com horario Onibus*/
                    conexao = connectarHorarioOnibus(URL);
                    break;


                default:
                    break;
            }

            int resposta = conexao.getResponseCode();
            if (resposta == HttpURLConnection.HTTP_OK) {
                InputStream is = conexao.getInputStream();
                JSONObject json = new JSONObject(bytesParaString(is));
                return json;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /*Utilizado para fazer a bufferização dos dados vindo pela rede*/
    private static String bytesParaString(InputStream is) throws IOException {
        byte[] buffer = new byte[1024];
        // O bufferzao vai armazenar todos os bytes lidos
        ByteArrayOutputStream bufferzao = new ByteArrayOutputStream();
        // precisamos saber quantos bytes foram lidos
        int bytesLidos;
        // Vamos lendo de 1KB por vez...
        while ((bytesLidos = is.read(buffer)) != -1) {
            // copiando a quantidade de bytes lidos do buffer para o bufferzão
            bufferzao.write(buffer, 0, bytesLidos);
        }
        return new String(bufferzao.toByteArray(), "UTF-8");
    }

    private static HttpURLConnection connectarSettings(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        java.net.URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    private static HttpURLConnection connectarHorarioOnibus(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        java.net.URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }




}
