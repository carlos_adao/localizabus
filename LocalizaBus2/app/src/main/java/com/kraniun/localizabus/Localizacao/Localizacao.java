package com.kraniun.localizabus.Localizacao;

import java.io.Serializable;

/**
 *
 */

public class Localizacao implements Serializable {
    int id;
    int cod;
    String att;
    double lat;
    double log;
    String provider;

    public Localizacao() {

    }


    public Localizacao(int id, int cod, String att, double lat, double log, String provider) {
        this.id = id;
        this.cod = cod;
        this.att = att;
        this.lat = lat;
        this.log = log;
        this.provider = provider;
    }

    public Localizacao(int cod, String att, double lat, double log, String provider) {
        this.id = id;
        this.cod = cod;
        this.att = att;
        this.lat = lat;
        this.log = log;
        this.provider = provider;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getAtt() {
        return att;
    }

    public void setAtt(String att) {
        this.att = att;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLog() {
        return log;
    }

    public void setLog(double log) {
        this.log = log;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getJsonInsertLocalizacao(){
        return "{\n" +
                " \"cod\": \""+cod+"\",\n" +
                " \"att\": \""+att+"\",\n" +
                " \"lat\": \""+lat+"\",\n" +
                " \"log\": \""+log+"\",\n" +
                " \"provider\": \""+provider+"\"\n" +
                " }";

    }

    @Override
    public String toString() {
        return "Localizacao{" +
                "id=" + id +
                ", cod=" + cod +
                ", att='" + att + '\'' +
                ", lat=" + lat +
                ", log=" + log +
                ", provider='" + provider + '\'' +
                '}';
    }
}
