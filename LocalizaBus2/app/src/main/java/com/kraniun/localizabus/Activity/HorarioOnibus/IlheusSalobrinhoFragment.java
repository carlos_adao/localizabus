package com.kraniun.localizabus.Activity.HorarioOnibus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kraniun.localizabus.Activity.MapActivity;
import com.kraniun.localizabus.BancoDados.AtualizaTabelas.SqlHorarioOnibus;
import com.kraniun.localizabus.Fragment.BaseFragment;
import com.kraniun.localizabus.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class IlheusSalobrinhoFragment extends BaseFragment {
    RecyclerView mRecyclerViewHoraioOnibus;
    LinearLayoutManager mLayoutManager;
    int local;
    int tipo;
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    Date hora;
    String dataFormatada;
    int ida_ou_volta = 0; //ida 0 e volta 1
    ImageView iv_trocar;
    TextView tv_loc_saida, tv_loc_chegada;
    Animation shake, animation_click;
    Context context;
    Vibrator vibrator;
    LinearLayout ll_trocar;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_ilheus_salobrinho, container, false);


        this.context = getContext();
        hora = Calendar.getInstance().getTime();
        dataFormatada = sdf.format(hora);

        iv_trocar = (ImageView) view.findViewById(R.id.iv_trocar);
        iv_trocar.setOnClickListener(clickInIcones);
        tv_loc_chegada = (TextView) view.findViewById(R.id.tv_loc_chegada);
        tv_loc_saida = (TextView) view.findViewById(R.id.tv_loc_saida);


        ll_trocar = (LinearLayout) view.findViewById(R.id.ll_trocar);
        ll_trocar.setOnClickListener(clickInIcones);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerViewHoraioOnibus = (RecyclerView) view.findViewById(R.id.recycler_view_horario_onibus);
        mRecyclerViewHoraioOnibus.setHasFixedSize(true);

        setHorariosLocal();

        shake = AnimationUtils.loadAnimation(context, R.anim.animation_swing);
        animation_click = AnimationUtils.loadAnimation(context, R.anim.animation_click);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // Lê o tipo dos argumentos.
            this.tipo = getArguments().getInt("tipo");
            this.local = getArguments().getInt("local");
        }
    }

    //Método usuado para evento de click nos icone
    private View.OnClickListener clickInIcones = new View.OnClickListener() {
        public void onClick(View v) {


            switch (v.getId()) {

                case R.id.iv_trocar:
                    executa();

                case R.id.ll_trocar:
                    executa();

                default:
                    break;
            }
        }
    };



    public void executa() {

        hora = Calendar.getInstance().getTime();
        dataFormatada = sdf.format(hora);

        //  setVibrator(iv_trocar);


        if (ida_ou_volta == 0) {
            ida_ou_volta = 1;

            if (local == 0) {
                tv_loc_chegada.setText("Ilhéus");
                tv_loc_saida.setText("UESC");
            } else if (local == 1) {
                tv_loc_chegada.setText("Itabuna");
                tv_loc_saida.setText("UESC");
            }

            switch (tipo) {
                case 0:

                   // mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(Comandos.retornaListaHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext()));

                    break;

                case 1:

                    // mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(Comandos.retornaListaHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext()));

                    break;

                case 2:

                    //mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(Comandos.retornaListaHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext()));
                    break;

                default:
                    break;
            }
        } else {
            ida_ou_volta = 0;

            if (local == 0) {
                tv_loc_chegada.setText("UESC");
                tv_loc_saida.setText("Ilhéus");
            } else if (local == 1) {
                tv_loc_chegada.setText("UESC");
                tv_loc_saida.setText("Itabuna");
            }

            switch (tipo) {
                case 0:

                    //mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(Comandos.retornaListaHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext()));
                    break;

                case 1:

                    //mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(Comandos.retornaListaHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext()));
                    break;

                case 2:

                    // mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(Comandos.retornaListaHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext()));
                    break;

                default:
                    break;
            }
        }
    }

    public void setHorariosLocal() {

        /*carrega a lista com os horários iniciais saída ilhéus x uesc*/
        tv_loc_saida.setText(getString(R.string.c1));
        tv_loc_chegada.setText(getString(R.string.i1));
        mRecyclerViewHoraioOnibus.setLayoutManager(mLayoutManager);
        mRecyclerViewHoraioOnibus.setItemAnimator(new DefaultItemAnimator());


        switch (tipo) {
            case 0:

                mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(SqlHorarioOnibus.SelectHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext(), onClickLho()));
                break;

            case 1:

                mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(SqlHorarioOnibus.SelectHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext(), onClickLho()));
                break;

            case 2:

                mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(SqlHorarioOnibus.SelectHorarioOnibus(ida_ou_volta, dataFormatada, tipo, local), getContext(), onClickLho()));
                break;

            default:
                break;
        }


    }

    public void setVibrator(ImageView iv) {

        iv.startAnimation(animation_click);
        vibrator.vibrate(30);
    }

    // metodo usado para pegar a linha que foi clicada do recicler professor
    private ListaHorarioOnibusAdapter.LhoOnClickListener onClickLho() {
        return new ListaHorarioOnibusAdapter.LhoOnClickListener() {
            @Override
            public void onClickLho(View view, String codigo) {
                Intent intent = new Intent(getActivity(), MapActivity.class);
                intent.putExtra("cod", codigo);
                startActivity(intent);
            }

        };
    }

}
