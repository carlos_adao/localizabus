package com.kraniun.localizabus.BancoDados.AtualizaTabelas;

import android.database.Cursor;

import com.kraniun.localizabus.BancoDados.Comandos;
import com.kraniun.localizabus.Objetos.Settings;

public class SqlSettings {

    /*verifica se tem dados na tabela de configuração*/
    public static boolean hasDados() {
        String query;
        Cursor cursor = null;

        query = "SELECT * FROM settings";
        cursor = Comandos.bd.rawQuery(query, null);

        if (!cursor.moveToNext()) {
            return  false;
        }

        return true;
    }

    /*insere dados na tabela de configuração*/
    public static void insert(Settings settingsRede) {

        /*Insert para iniciar a tabela de configuranção*/
        String Es = "INSERT INTO settings (cod," +
                "att_funcionario," +
                "att_professor," +
                "att_sala," +
                "att_semestre_virgente," +
                "att_disciplina," +
                "att_ementa," +
                "att_pre_requisito," +
                "att_disciplina_dia_hora," +
                "att_horario_onibus)VALUES (" +
                "'21-09-2017'," +
                "'21-09-2017'," +
                "'21-09-2017'," +
                "'21-09-2017'," +
                "'21-09-2017'," +
                "'21-09-2017'," +
                "'21-09-2017'," +
                "'21-09-2017'," +
                "'21-09-2017'," +
                "'" + settingsRede.getAtt_horario_onibus() + "')";
        String[] i = {Es};
        Comandos.executeSQL(i);

    }

    /*Atualiza dados da tabela de configurações*/
    public static void update(Settings settingsRede) {

        String ud = "UPDATE settings SET cod = '" + settingsRede.getCod() + "'" +
                ", att_funcionario = '" + settingsRede.getAtt_funcionario() + "'" +
                ", att_professor = '" + settingsRede.getAtt_professor() + "'" +
                ", att_sala = '" + settingsRede.getAtt_sala() + "'" +
                ", att_semestre_virgente = '" + settingsRede.getAtt_semestre_virgente() + "'" +
                ", att_disciplina = '" + settingsRede.getAtt_disciplina() + "'" +
                ", att_ementa = '" + settingsRede.getAtt_ementa() + "'" +
                ", att_pre_requisito = '" + settingsRede.getAtt_pre_requisito() + "'" +
                ", att_disciplina_dia_hora = '" + settingsRede.getAtt_disciplina_dia_hora() + "'" +
                ", att_horario_onibus = '" + settingsRede.getAtt_horario_onibus() + "'" +
                "  WHERE cod = '" + settingsRede.getCod() + "' ;";

        String[] u = {ud};
        Comandos.executeSQL(u);
    }

    /*deleta todos os dados da tabela de configuração*/
    public static String[] delete() {

        String comando = "DELETE FROM settings";
        String[] d = {comando};
        return d;
    }

    /*Retorna todos os dados da tabela de configuração*/
    public static Settings select() {
        Settings settings = null;

        final String query = "SELECT cod, att_funcionario, att_professor,att_sala,att_semestre_virgente,att_disciplina,att_ementa,att_pre_requisito,att_disciplina_dia_hora, att_horario_onibus" +
                " FROM settings";

        Cursor cursor = Comandos.bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("cod");
            int b = cursor.getColumnIndex("att_funcionario");
            int c = cursor.getColumnIndex("att_professor");
            int d = cursor.getColumnIndex("att_sala");
            int e = cursor.getColumnIndex("att_semestre_virgente");
            int f = cursor.getColumnIndex("att_disciplina");
            int g = cursor.getColumnIndex("att_ementa");
            int h = cursor.getColumnIndex("att_pre_requisito");
            int i = cursor.getColumnIndex("att_disciplina_dia_hora");
            int j = cursor.getColumnIndex("att_horario_onibus");


            settings = new Settings();
            settings.setCod(cursor.getString(a));
            settings.setAtt_funcionario(cursor.getString(b));
            settings.setAtt_professor(cursor.getString(c));
            settings.setAtt_sala(cursor.getString(d));
            settings.setAtt_semestre_virgente(cursor.getString(e));
            settings.setAtt_disciplina(cursor.getString(f));
            settings.setAtt_ementa(cursor.getString(g));
            settings.setAtt_disciplina_dia_hora(cursor.getString(i));
            settings.setAtt_horario_onibus(cursor.getString(j));

        }

        return settings;
    }


}
