package com.kraniun.localizabus.Home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.kraniun.localizabus.R;

public class DialogOpcaoLinha extends Activity {
    CardView card_salobrinho;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_opcao_linha);
        vinculaComponentes();
    }

    //metodo usado para vincular os componentes xml ao java
    public void vinculaComponentes(){
        card_salobrinho = (CardView) findViewById(R.id.card_salobrinho);
        addListenerComponets();
    }

    /*Método usado para vincular esculta de clicks nos compomentes*/
    private void  addListenerComponets(){
        card_salobrinho.setOnClickListener(clickComponets);
    }

    /*metodo usado para capturar os eventos de clicks*/
    private View.OnClickListener clickComponets = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.card_salobrinho:
                    String message="salobrinho";
                    Intent intent = new Intent();
                    intent.putExtra("MESSAGE",message);
                    setResult(1,intent);
                    finish();//finishing activity
                    break;

                default:
                    break;
            }
        }
    };
}
