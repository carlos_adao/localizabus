package com.kraniun.localizabus.Activity.HorarioOnibus;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.kraniun.localizabus.Activity.BaseActivity;
import com.kraniun.localizabus.Coneccao.SettingsTask;
import com.kraniun.localizabus.R;

public class HorarioOnibusActivity extends BaseActivity {

    private String titulo;
    int local;
    Bundle savedInstanceState;
    IlheusSalobrinhoFragment frag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario_onibus);
        this.savedInstanceState = savedInstanceState;

        titulo = getString(R.string.ta);//pega dos recursos o titulo da activity
        local = 0;

        setUpToolbar();
        downloadSettings();

        iniciaFragmentoHorarioOnibus(savedInstanceState, titulo, local);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_horario_onibus, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (local == 0) {
            if (id == R.id.addProximos) {
                if (savedInstanceState == null) {
                    getSupportFragmentManager().beginTransaction().remove(frag).commit();
                    Bundle args = new Bundle();
                    args.putInt("local", 0);
                    args.putInt("tipo", 0);
                    IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
                    f.setArguments(args);

                    this.frag = f;
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
                    transaction.commit();

                }
            } else if (id == R.id.jaSairam) {


                if (savedInstanceState == null) {

                    getSupportFragmentManager().beginTransaction().remove(frag).commit();
                    Bundle args = new Bundle();
                    args.putInt("local", 0);
                    args.putInt("tipo", 1);
                    IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
                    f.setArguments(args);

                    this.frag = f;
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
                    transaction.commit();
                }

            } else if (id == R.id.todos) {


                if (savedInstanceState == null) {

                    getSupportFragmentManager().beginTransaction().remove(frag).commit();
                    Bundle args = new Bundle();
                    args.putInt("local", 0);
                    args.putInt("tipo", 2);
                    IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
                    f.setArguments(args);

                    this.frag = f;
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
                    transaction.commit();


                }
            }
        } else if (local == 1) {
            if (id == R.id.addProximos) {
                if (savedInstanceState == null) {
                    getSupportFragmentManager().beginTransaction().remove(frag).commit();
                    Bundle args = new Bundle();
                    args.putInt("local", 1);
                    args.putInt("tipo", 0);
                    IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
                    f.setArguments(args);

                    this.frag = f;
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
                    transaction.commit();

                }
            } else if (id == R.id.jaSairam) {


                if (savedInstanceState == null) {

                    getSupportFragmentManager().beginTransaction().remove(frag).commit();
                    Bundle args = new Bundle();
                    args.putInt("local", 1);
                    args.putInt("tipo", 1);
                    IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
                    f.setArguments(args);

                    this.frag = f;
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
                    transaction.commit();
                }

            } else if (id == R.id.todos) {


                if (savedInstanceState == null) {

                    getSupportFragmentManager().beginTransaction().remove(frag).commit();
                    Bundle args = new Bundle();
                    args.putInt("local", 1);
                    args.putInt("tipo", 2);
                    IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
                    f.setArguments(args);

                    this.frag = f;
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
                    transaction.commit();


                }
            }
        }
        return super.onOptionsItemSelected(item);
    }


    //Inicia o fragmento  que contem a historia
    public void iniciaFragmentoHorarioOnibus(Bundle savedInstanceState, String titulo, int local) {

        getSupportActionBar().setTitle(titulo);

        // Adiciona o fragment no layout
        if (savedInstanceState == null) {

            Bundle args = new Bundle();
            args.putInt("local", 0);//define a cidade do intinerario
            args.putInt("tipo", 0);//define qual tipo de viagem
            IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
            f.setArguments(args);
            this.frag = f;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
            transaction.commit();

        }
    }


    /*Inicia o objeto que efetua o download da tabela de configuração*/
    private void downloadSettings() {


        SettingsTask task = new SettingsTask();
        task.execute();

    }

}
