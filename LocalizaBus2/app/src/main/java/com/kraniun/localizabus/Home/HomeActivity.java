package com.kraniun.localizabus.Home;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.kraniun.localizabus.Activity.BaseActivity;
import com.kraniun.localizabus.Auxiliares.PermissionUtils;
import com.kraniun.localizabus.Localizacao.Localizacao;
import com.kraniun.localizabus.Localizacao.LocalizacaoControle;
import com.kraniun.localizabus.Mapa.MapActivity;
import com.kraniun.localizabus.R;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private Button btn_fornecer_localizacao, btn_receber_localizacao;
    private TextView tv_lat, tv_long;
    private GoogleApiClient mGoogleApiClient;
    private String TAG = "Home";
    private String[] permissoes = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    private LocalizacaoControle lc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setUpToolbar();
        configuraAPIGooogle();
        PermissionUtils.validate(this, 0, permissoes);//solicita as permissões para acessar a localização
        vinculaXmlComJava();
        addListenerButton();
    }

    /*vincula os componentes java com os respectivos xmls*/
    private void vinculaXmlComJava() {
        lc = new LocalizacaoControle();
        btn_fornecer_localizacao = findViewById(R.id.btn_fornecer_localizacao);
        btn_receber_localizacao = findViewById(R.id.btn_receber_localizacao);
        tv_lat = (TextView) findViewById(R.id.tv_lat);
        tv_long = (TextView) findViewById(R.id.tv_log);
    }

    /*adiciona listen no botão*/
    private void addListenerButton() {
        btn_fornecer_localizacao.setOnClickListener(clickButtons);
        btn_receber_localizacao.setOnClickListener(clickButtons);
    }

    /*evento de click no botão */
    public View.OnClickListener clickButtons = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.btn_fornecer_localizacao:
                    chamaDialogOpcaoLinha();
                    break;
                case R.id.btn_receber_localizacao:
                    receberLocalizacao();
                    break;

                default:
                    break;
            }
        }
    };


    /*Método usado para receber uma localização através do mapa activity*/
    private void receberLocalizacao() {
        Intent intent = new Intent(getActivity(), MapActivity.class);
        intent.putExtra("cod", "28");
        startActivity(intent);
    }

    /*Configura o google API clint para utilizar o mapa*/
    private void configuraAPIGooogle() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /*verifica o resultado das permissões solicitadas ao usuario*/
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for (int result : grantResults) {
            if (result == PackageManager.PERMISSION_DENIED) {
                alertAndFinish();
                return;
            }
        }
    }

    /*Exibe um alerta infomando que é necessario aceitar as permissões*/
    private void alertAndFinish() {
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.app_name).setMessage("Para utilizar este aplicativo, você precisa aceitar as permissões.");
            // Add the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    finish();
                }
            });
            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();

        }
    }

    /*Chama o dialog que escolhe a opção da linha*/
    private void chamaDialogOpcaoLinha() {
        Intent intent = new Intent(getContext(), DialogOpcaoLinha.class);
        startActivityForResult(intent, 1);
    }

    /*Método usado para escultar a activity que foi chamada*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            String linha = data.getStringExtra("MESSAGE");
            iniciaEnvioLocalizacao(linha);
        }
    }

    //Método que será usado para evia a localização para o servidor
    public void iniciaEnvioLocalizacao(String linha) {
        Toast.makeText(getApplicationContext(), "Enviando localizacao" + linha, Toast.LENGTH_SHORT).show();
        startLocationUpdates();//inicia a lemitura do GPS
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Conecta no Google Play Services
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        //Para o GPS
        stopLocationUpdates();
        //desconecta no Google Play Services
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Conectado ao google play service");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Conecção interrompida com o  google play service");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Falha ao se conectar com o google play service");
    }

    /*Método executado quando a localização muda*/
    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, " Executando o método location");
        enviaLocalizacaoServidor(location);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    /*Método usado para iniciar a leitura do gps*/
    protected void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates()");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1 * 1000);
        mLocationRequest.setFastestInterval(1 * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        Log.d(TAG, "stopLocationUpdates()");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private void enviaLocalizacaoServidor(Location loc) {
        Log.i(TAG, " Enviando localização");
        Toast.makeText(this, "Enviando localizacao", Toast.LENGTH_SHORT).show();
        printTvLatLong(loc);
    }

    private void printTvLatLong(Location loc) {
        lc.enviaLocalizacao(new Localizacao(28, getData(), loc.getLatitude(), loc.getLongitude(), "provider"));
        tv_lat.setText(String.valueOf(loc.getLatitude()));
        tv_long.setText(String.valueOf(loc.getLongitude()));
    }

    //Método usado para retornar a data atual do android
    private String getData() {
        SimpleDateFormat formataData = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date data = new Date();
        return formataData.format(data);
    }

}
