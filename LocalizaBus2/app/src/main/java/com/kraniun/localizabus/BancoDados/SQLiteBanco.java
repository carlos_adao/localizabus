package com.kraniun.localizabus.BancoDados;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class SQLiteBanco extends SQLiteOpenHelper {

    private static final String cat = "SQL";
    private String[] scriptSQLCreate;
    private String scriptSQLDelete;
    SQLiteDatabase bd;

    public SQLiteBanco(Context context, String nomeBanco, int versaoBanco, String[] scriptSQLCreate) {
        super(context, nomeBanco, null, versaoBanco);
        this.scriptSQLCreate = scriptSQLCreate;
        Log.i(cat, "Chegando no Construtor");
    }

    @Override
    public void onCreate(SQLiteDatabase bd) {
        int qtdScripts = scriptSQLCreate.length;
        //Executa cada SQL passado como padrão
        for(int i = 0; i < qtdScripts;i++){
            String sql = scriptSQLCreate[i];
            Log.i(cat, sql);
            //Cria um banco de dados usando script de criação
            bd.execSQL(sql);
        }
        this.bd = bd;
    }

    @Override
    public void onUpgrade(SQLiteDatabase bd, int i, int i1) {
    }
}



