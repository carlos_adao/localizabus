package com.kraniun.localizabus.BancoDados;

import android.content.Context;
import android.util.Log;

public class Acesso extends Comandos {

    private static final String tag = "sql";
    public static final String nome_banco = "LOCALIZABUS.sqlite";
    private static final int versao_banco = 1;
    private SQLiteBanco bdHelper;

    public Acesso(Context context) {

        bdHelper = new SQLiteBanco(context, nome_banco, versao_banco, Comandos.createTable());
        //Abre o banco no modo leitura para poder alterar também
        bd = bdHelper.getWritableDatabase();

    }

    public void criaBD(Context context){
        bdHelper = new SQLiteBanco(context, nome_banco, versao_banco, Comandos.createTable());
        //Abre o banco no modo leitura para poder alterar também
        bd = bdHelper.getWritableDatabase();

    }


    public void fecha() {
        super.fechar();
        if (bdHelper != null) {
            bdHelper.close();
        }
    }

}
