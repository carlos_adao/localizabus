package com.kraniun.localizabus.Localizacao.Service;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Button;

import com.kraniun.localizabus.Localizacao.Localizacao;

import java.util.List;

public class LocalizacaoControle {

    public LocalizacaoControle() {}
    String TAG = "Localizacao Controller";

    public void insert(Localizacao l, Context context) {

        try {
            boolean result = new HttpRequestAdd().execute(l).get();
            Log.i("result", String.valueOf(result));
        } catch (Exception e) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
    }

    public void upgrade(Localizacao l, Context context) {

        try {
            boolean result = new HttpRequestUpgrade().execute(l).get();
            Log.i("result", String.valueOf(result));
        } catch (Exception e) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(e.getMessage());
            builder.create().show();
        }
    }

    public Localizacao getLocalizacaoId(Integer id) {
        Log.i(TAG, "Tentativa de obter uma localização");
        try {
            return new HttpRequestFind().execute(id).get();

        } catch (Exception e) {
            Log.i("Info", "Localização não encontrada");
            return null;
        }
    }

    public List<Localizacao> getLocalizacao() {

        try {
            return new HttpRequestFindAll().execute().get();
        } catch (Exception e) {

            Log.i("Info", "Localização não encontrada");
            return null;
        }
    }

    /*classe que execulta a thread em background para o metodo get*/
    private class HttpRequestFindAll extends AsyncTask<Void, Void, List<Localizacao>> {


        @Override
        protected List<Localizacao> doInBackground(Void... voids) {

            LocalizacaoModel localizacaoModel = new LocalizacaoModel();
            return localizacaoModel.findAll();
        }

        @Override
        protected void onPostExecute(List<Localizacao> localizacaoList) {
            super.onPostExecute(localizacaoList);
        }
    }

    /*classe que execulta a thread em background para o metodo get*/
    private class HttpRequestFind extends AsyncTask<Integer, Void, Localizacao> {

        @Override
        protected Localizacao doInBackground(Integer... params) {
            LocalizacaoModel localizacaoModel = new LocalizacaoModel();
            return localizacaoModel.localizacaoFind(params[0]);
        }

        @Override
        protected void onPostExecute(Localizacao localizacao) {
            super.onPostExecute(localizacao);
        }
    }


    /*class executada em background para o metodo post*/
    private class HttpRequestAdd extends AsyncTask<Localizacao, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Localizacao... localizacaos) {
            LocalizacaoModel localizacaoModel = new LocalizacaoModel();

            return localizacaoModel.create(localizacaos[0]);
        }
    }

    /*class executada em background para o metodo post*/
    private class HttpRequestUpgrade extends AsyncTask<Localizacao, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Localizacao... localizacaos) {
            LocalizacaoModel localizacaoModel = new LocalizacaoModel();

            return localizacaoModel.upgrade(localizacaos[0]);
        }
    }

    /*class executada em background para o metodo post*/
    private class HttpRequestDelete extends AsyncTask<Localizacao, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

        @Override
        protected Boolean doInBackground(Localizacao... localizacaos) {
            LocalizacaoModel localizacaoModel = new LocalizacaoModel();
            return localizacaoModel.create(localizacaos[0]);
        }
    }

}
