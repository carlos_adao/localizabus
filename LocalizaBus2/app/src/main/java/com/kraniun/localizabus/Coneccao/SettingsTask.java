package com.kraniun.localizabus.Coneccao;

import android.os.AsyncTask;
import android.util.Log;

import com.kraniun.localizabus.Atualizacao.GetDadosServidor;
import com.kraniun.localizabus.BancoDados.AtualizaTabelas.SqlSettings;
import com.kraniun.localizabus.Objetos.Settings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class SettingsTask extends AsyncTask<Void, Void, JSONObject> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(Void... strings) {
        JSONObject j = null;
        try {
            ConexaoHttp.setURL("http://13.58.21.219/colcicapi/settings");
            j = ConexaoHttp.carregarArquivoJson(1);
        } catch (Exception e) {
            Log.e("Erro Download", e.getMessage(), e);
        }
        return j;

    }

    protected void onPostExecute(JSONObject arquivoJson) {
        Settings settings;


        if (arquivoJson != null) {
            try {

                settings = convertJsonParaSettings(String.valueOf(arquivoJson));
                atualizaTabelas(settings);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Settings convertJsonParaSettings(String json) throws IOException {

        Settings settings = null;
        try {
            JSONObject object = new JSONObject(json);
            JSONArray jsonSettings = object.getJSONArray("Setting");

            JSONObject jsonset = jsonSettings.getJSONObject(0);

            //Lê as informações de settings
            settings = new Settings();
            settings.setCod(jsonset.optString("cod"));
            settings.setAtt_funcionario(jsonset.optString("att_funcionario"));
            settings.setAtt_professor(jsonset.optString("att_professor"));
            settings.setAtt_sala(jsonset.optString("att_sala"));
            settings.setAtt_semestre_virgente(jsonset.optString("att_semestre_virgente"));
            settings.setAtt_disciplina(jsonset.optString("att_disciplina"));
            settings.setAtt_ementa(jsonset.optString("att_ementa"));
            settings.setAtt_pre_requisito(jsonset.optString("att_pre_requisito"));
            settings.setAtt_disciplina_dia_hora(jsonset.optString("att_disciplina_dia_hora"));
            settings.setAtt_horario_onibus(jsonset.optString("att_horario_onibus"));

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }

        return settings;
    }

    public void atualizaTabelas(Settings settingsRede) {
        Settings settingsLocal = SqlSettings.select();// pega a tabela de configuração


        if (!SqlSettings.hasDados()) {

            Log.i("LOG", "A tabela settings não está preenchida!!!");

            executa(settingsRede);


        } else {

            Log.i("LOG", "A tabela settings está preenchida!!!");

            if (!(settingsRede.getAtt_horario_onibus().equalsIgnoreCase(settingsLocal.getAtt_horario_onibus()))) {

                executa(settingsRede);

            }
        }

    }

    /*  inicia uma instância do objeto que atualiza os dados
    *   atualiza a data na tabela de configuração         */
    public void executa(Settings settingsRede) {
        GetDadosServidor att;

        /*Instância um objeto para realizar o download dos dados para atualiza a tabela*/
        att = new GetDadosServidor();
        att.setTipo(1);
        att.execute();

        /*atualiza a data na tabela de configuração*/
        SqlSettings.update(settingsRede);

    }
}