package com.kraniun.localizabus.Atualizacao;

import android.os.AsyncTask;
import android.util.Log;

import com.kraniun.localizabus.BancoDados.Comandos;
import com.kraniun.localizabus.Coneccao.ConexaoHttp;
import com.kraniun.localizabus.Objetos.HorarioOnibus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by estagio-nit on 22/09/17.
 */

public class GetDadosServidor extends AsyncTask<Void, Void, JSONObject> {
    int tipo;

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected JSONObject doInBackground(Void... strings) {
        JSONObject j = null;
        try {

            switch (tipo) {
                case 1:
                    /*Atualizar o horario dos onibus*/
                    ConexaoHttp.setURL("http://13.58.21.219/colcicapi/horario_onibus");
                    return (ConexaoHttp.carregarArquivoJson(1));

                default:
                    break;
            }


        } catch (Exception e) {
            Log.e("Erro Download", e.getMessage(), e);
        }

        return j;
    }

    protected void onPostExecute(JSONObject arquivoJson) {

        String data = String.valueOf(arquivoJson);

        if (arquivoJson != null) {
            try {

                switch (tipo) {
                    case 1:

                        ArrayList<HorarioOnibus> lHorOnibus = jsonForHorarioOnibus(data);
                        Comandos.atualizaTabelaHorarioOnibus(lHorOnibus);
                        break;

                    default:
                        break;
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    private ArrayList<HorarioOnibus> jsonForHorarioOnibus(String json) throws IOException {
        ArrayList<HorarioOnibus> listHorarioOnibus = new ArrayList<>();
        HorarioOnibus ho = null;

        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonHorarioOnibus = object.getJSONArray("Horario_onibus");


            for (int i = 0; i < jsonHorarioOnibus.length(); i++) {
                JSONObject jsonset = jsonHorarioOnibus.getJSONObject(i);

                // Lê as informações da lista de horarios dos onibus
                ho = new HorarioOnibus();
                ho.setCod(jsonset.optString("cod"));
                ho.setNum_linha(jsonset.optString("num_linha"));
                ho.setVia(jsonset.optString("via"));
                ho.setLoc_saida(jsonset.optString("loc_saida"));
                ho.setLoc_chegada(jsonset.optString("loc_chegada"));
                ho.setCidade(jsonset.optString("cidade"));
                ho.setEmpresa(jsonset.optString("empresa"));
                ho.setHora_saida(jsonset.optString("hora_saida"));
                ho.setStatus(jsonset.optString("status"));


                listHorarioOnibus.add(ho);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }

        return listHorarioOnibus;
    }

}
