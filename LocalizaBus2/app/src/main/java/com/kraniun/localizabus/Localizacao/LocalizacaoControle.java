package com.kraniun.localizabus.Localizacao;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

public class LocalizacaoControle{
    private static String TAG = "localizacao controller";


    public LocalizacaoControle() { }


    /*Método usado para enviar localizacao para o servidor*/
    public Integer enviaLocalizacao(Localizacao loc){
        try {
            int result = new HttpRequestEnviaLocalizacao().execute(loc).get();
            return result;

        } catch (Exception e) {

            Log.i(TAG, "Erro dentro do catch");
            return 0;//Abreviação para falha na conexão
        }
    }

    /*Método usado para receber a localização do servidro*/
    public Localizacao recebeLocalizacao(String linha){
        try {
            Localizacao result = new HttpRequestRecebeLocalizacao().execute(linha).get();
            return result;

        } catch (Exception e) {
            Log.i(TAG, "Erro dentro do catch");
            return null;//Abreviação para falha na conexão
        }
    }


    /*Método usado para receber todas as localizações de um determinado dia*/
    public ArrayList<Localizacao> recebeAllLocalizacao(String linha){
        try {
            ArrayList<Localizacao> result = new HttpRequestRecebeAllLocalizacao().execute(linha).get();
            return result;

        } catch (Exception e) {
            Log.i(TAG, "Erro dentro do catch");
            return null;//Abreviação para falha na conexão
        }
    }

    /*class executada em background para o metodo post*/
    private class HttpRequestEnviaLocalizacao extends AsyncTask<Localizacao, Void, Integer> {
        @Override
        protected void onPostExecute( Integer aBoolean) {
            super.onPostExecute(aBoolean);
        }
        @Override
        protected Integer doInBackground(Localizacao... loc) {
            LocalizacaoModel lm = new LocalizacaoModel();
                int result = lm.enviaLocalizacaoInWS(loc[0]);
            return 1;
        }
    }

    /*class executada em background para o metodo post receber localização*/
    private class HttpRequestRecebeLocalizacao extends AsyncTask<String, Void, Localizacao> {
        @Override
        protected void onPostExecute( Localizacao aBoolean) {
            super.onPostExecute(aBoolean);
        }
        @Override
        protected Localizacao doInBackground(String... linha) {
            LocalizacaoModel lm = new LocalizacaoModel();
            Localizacao result = lm.recebeLocalizacaoInWS(linha[0]);
            return result;
        }
    }

    /*class executada em background para o metodo get que recebe todas as posições de um determinado dia*/
    private class HttpRequestRecebeAllLocalizacao extends AsyncTask<String, Void, ArrayList<Localizacao>> {
        @Override
        protected void onPostExecute(ArrayList<Localizacao> aBoolean) {
            super.onPostExecute(aBoolean);
        }
        @Override
        protected ArrayList<Localizacao> doInBackground(String... linha) {
            LocalizacaoModel lm = new LocalizacaoModel();
            ArrayList<Localizacao> result = lm.recebeAllLocalizacaoInWS(linha[0]);
            return result;
        }
    }

}
