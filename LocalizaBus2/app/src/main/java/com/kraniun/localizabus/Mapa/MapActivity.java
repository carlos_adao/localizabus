package com.kraniun.localizabus.Mapa;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kraniun.localizabus.Localizacao.Localizacao;
import com.kraniun.localizabus.Localizacao.LocalizacaoControle;
import com.kraniun.localizabus.R;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocalizacaoControle lc;
    private String TAG = "Map Activity";
    private Thread thread;
    private MarkerOptions markerOptions;
    ArrayList<Localizacao>  l;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        lc = new LocalizacaoControle();
        markerOptions = new MarkerOptions();
        run2();

    }

    @Override
    protected void onStop() {
        super.onStop();
        thread.interrupt();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Configura o tipo do mapa
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    /*adiciona o posicionamento do cursor no mapa*/
    public void setPositionMap(Localizacao loc) {
        runOnUiThread(new Runnable(){
            public void run() {
                Toast.makeText(getApplicationContext(), "Status " , Toast.LENGTH_LONG).show();
            }
        });
        if (loc != null) {
            if(mMap != null){
                final LatLng latLng = new LatLng(loc.getLat(), loc.getLog());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        markerOptions.position(latLng).title("Terminal Urbano").snippet("Via Cidade Nova");
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_bus));
                        mMap.addMarker(markerOptions);
                        // Um zoom no mapa para a seua posição atual...
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                    }
                });
            }
        }
    }


    /*Método usado para obter a ultima localização da linha cadastrada no servidor*/
    private void getLocation() {
        Localizacao l = lc.recebeLocalizacao("28");
        setPositionMap(l);
    }

    /*Método usado para receber todas as localizaç~es de um determinado dia*/
    private void getAllLocation() {
         l = lc.recebeAllLocalizacao("28");

        thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (true) {
                        setPositionMap(l.get(i));
                        i = i + 10;

                        sleep(2000);
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        };
        thread.start();
    }

    public void run() {
         thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (true) {
                        getLocation();
                        sleep(100);
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        };
        thread.start();
    }

    public void run2() {
        thread = new Thread() {
            @Override
            public void run() {
                getAllLocation();
            }
        };

        thread.start();
    }



}
