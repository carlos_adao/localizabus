package com.kraniun.localizabus.BancoDados;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.kraniun.localizabus.BancoDados.AtualizaTabelas.SqlHorarioOnibus;
import com.kraniun.localizabus.Objetos.HorarioOnibus;
import com.kraniun.localizabus.Objetos.Settings;
import com.kraniun.localizabus.Objetos.StatusTabelas;

import java.util.ArrayList;




/**
 * Created by carlos on 26/04/2017.
 */

public class Comandos {
    public static SQLiteDatabase bd;

    public void fechar() {
        //fecha o banco de dados
        if (bd != null) {
            bd.close();
        }
    }

    public static boolean dropDataBase(Context context){
        return  context.deleteDatabase("LOCALIZABUS.sqlite"); // true if deleted
    }

    public static boolean executeSQL(String[] script) {

        for (int i = 0; i < script.length; i++) {
            String sql = script[i];
            Log.i("SQL", sql);
            //Cria um banco de dados usando script de criação
            bd.execSQL(sql);
        }

        return true;
    }

    public static String[] createTable() {


        /*Script para criação da tabela de horario dos ônibus*/
        String horario_onibus = "CREATE TABLE IF NOT EXISTS horario_onibus(\n" +
                "   cod VARCHAR(10)PRIMARY KEY, \n" +
                "   num_linha VARCHAR(10) NOT NULL, \n" +
                "   via VARCHAR(50) NOT NULL,\n" +
                "   loc_saida VARCHAR(50) NOT NULL,\n" +
                "   loc_chegada VARCHAR(50) NOT NULL,\n" +
                "   cidade VARCHAR(20) NOT NULL,\n" +
                "   empresa VARCHAR(20) NOT NULL,\n" +
                "   hora_saida VARCHAR(10) NOT NULL,\n" +
                "   status VARCHAR(10) NOT NULL\n" +
                ")";

        //tabela usada excusivamente para atualização de dados
        String settings = "CREATE TABLE IF NOT EXISTS settings(\n" +
                "   cod VARCHAR(10)PRIMARY KEY, \n" +
                "   att_funcionario VARCHAR(10) NOT NULL, \n" +
                "   att_professor VARCHAR(10) NOT NULL, \n" +
                "   att_sala VARCHAR(10) NOT NULL,\n" +
                "   att_semestre_virgente VARCHAR(10) NOT NULL,\n" +
                "   att_disciplina VARCHAR(10) NOT NULL,\n" +
                "   att_ementa VARCHAR(10) NOT NULL,\n" +
                "   att_pre_requisito VARCHAR(10) NOT NULL,\n" +
                "   att_disciplina_dia_hora VARCHAR(10) NOT NULL," +
                "   att_horario_onibus VARCHAR(10) NOT NULL" +
                ")";



        String[] script = {horario_onibus, settings};

        return script;
    }

    public static void dropTable() {

        String D0 = "DROP TABLE settings";
        String D1 = "DROP TABLE horario_onibus";


        String[] script = {D0, D1};
        executeSQL(script);

    }


    public static void atualizaTabelaHorarioOnibus(ArrayList<HorarioOnibus> lHoSw) {

        SqlHorarioOnibus.run(lHoSw, bd);
    }

}
