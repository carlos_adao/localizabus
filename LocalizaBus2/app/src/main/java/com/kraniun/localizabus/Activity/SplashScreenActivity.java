package com.kraniun.localizabus.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import com.kraniun.localizabus.BancoDados.Acesso;
import com.kraniun.localizabus.Home.HomeActivity;
import com.kraniun.localizabus.R;

public class SplashScreenActivity extends AppCompatActivity {

    Acesso a;
    /*Inicia a tela de Splashscreen e em seguida a tela de home*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        iniciaBancoDados();
        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
               mostrarHome();
            }
        }, 2000);
    }

    /*Chama a tela inicial da aplicação*/
    private void mostrarHome() {
        Intent intent = new Intent(SplashScreenActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    /*Inicia o banco de dados local do android*/
    private void iniciaBancoDados(){
        a = new Acesso(getBaseContext());
        a.criaBD(getBaseContext());

    }
}